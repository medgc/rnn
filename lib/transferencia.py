import numpy as np
"""
Funciones de transferencia, permiten obtener el valor de f(x) y de su derivada para un x dado.
"""

class Lineal:
    def evaluar_f(self, x):
        """
        Devuelve el valor de la funcion f(x) = x.
        :param x: valor donde evaluar f.
        :return: valor de f en el punto x.
        """
        return x

    def evaluar_df(self, x):
        """
        Devuelve el valor de f'(x) para f(x) = x
        :param x: valor donde evaluar f'(x).
        :return: 1 (la derivada de una función lineal es una constante).
        """
        return 1

class TanSig:
    def evaluar_f(self, x):
        """
        Devuelve el valor de la funcion f(x) = [2 / (1 + e^(-2x))] - 1.
        :param x: valor donde evaluar f.
        :return: valor de f en el punto x.
        """
        return 2.0 / (1 + np.exp(np.dot(-2, x))) - 1

    def evaluar_df(self, x):
        """
        Devuelve el valor de f'(x) para f(x) = [2 / (1 + e^(-2x))] - 1.
        :param x: valor donde evaluar f'(x).
        :return: 1 - x^2
        """
        return 1 - x ** 2

class LogSig:
    def evaluar_f(self, x):
        """
        Devuelve el valor de la funcion f(x) = 1 / (1 + e^(-x))
        :param x: valor donde evaluar f.
        :return: valor de f en el punto x.
        """
        return 1.0 / (1 + np.exp(-x))

    def evaluar_df(self, x):
        """
        Devuelve el valor de f'(x) para f(x) = [2 / (1 + e^(-2x))] - 1.
        :param x: valor donde evaluar f'(x).
        :return: x * (1 - x)
        """
        return x * (1 - x)