import numpy as np
import logging

class Perceptron:
    weights = None
    b = None

    def __init__(self, cant_entradas, usa_b=False):
        """
        Inicialización del perceptron.
        :param cant_entradas: Cantidad de entradas del perceptron.
        :param usa_b: Indica si debe usar termino independiente.
        """
        # pongo los weights en 0
        self.weights = np.random.uniform(-0.5, 0.5, cant_entradas)
        if usa_b:
            self.b = np.random.uniform(-0.5, 0.5)

    def usa_b(self):
        """
        Indica si el perceptron utiliza el término independiente o no.
        :return: True si lo utiliza, False si no.
        """
        return self.b is not None

    def get_weights(self):
        """
        Devuelve el vector de pesos para cada entrada.
        :return: Vector de pesos.
        """
        return self.weights

    def get_b(self):
        """
        Devuelve el valor de b.
        :return: Valor de b.
        """
        return self.b

    def __str__(self):
        return 'Perceptron: {W: %s, b: %s}' % (
            np.array2string(np.asarray(self.weights), formatter={'float':lambda x: '%.2f' % x}),
            ('%f' % self.b) if self.usa_b() else 'N/A')


    def ajustar(self, alfa, features, pred, actual):
        """
        Ajusta los pesos del perceptron.
        :param alfa: Velocidad de entrenamiento.
        :param features: Vector de entrada (X[i])
        :param pred: predicción del perceptrón para la entrada.
        :param actual: valor esperado para la entrada.
        """
        error = actual - pred
        self.weights += alfa * error * features
        if self.usa_b():
            self.b += alfa * error

    def predecir(self, X):
        """
        Predice los y para cada elemento en X.
        :param X: Array de vectores donde cada componente es un feature de entrada.
        :return: Array de vectores donde cada elemento es el resultado predicho.
        """
        result = []

        for item in X:
            neta = np.dot(self.weights, item) + (self.b if self.usa_b() else 0)
            result.append([(neta > 0) * 1])

        return result


