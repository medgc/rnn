import numpy as np
import logging
from lib.neurona import Neurona
from lib.perceptron import Perceptron

class ReglaDelta:

    def __init__(self, max_ite=1000, error_ok=1.0e-05, alfa=0.05):
        """
        Inicilaización del entrenador del modelo por descenso de gradiente.
        :param max_ite: Cantidad de iteraciones máxima.
        :param error_ok: Máximo error tolerable antes de detener el entrenamiento.
        :param alfa: Valor de ajuste del gradiente en cada iteración.
        """
        self.max_ite = max_ite
        self.error_ok = error_ok
        self.alfa = alfa
        self.error_avg = 0.0


    def iterar_dataset(self, n, X, y):
        """
        Realiza una iteración de entrenamiento sobre el dataset.
        :param n: La neurona a entrenar.
        :param X: El conjunto de entrenamiento.
        :param y: El conjunto de clases para cada item en X.
        :return: El error promedio de la iteración.
        """
        error_acum = 0.0
        self.cambios = False

        # recorro el dataset
        for i in range(X.shape[0]):

            # calculo la predicción y la diferencia con el valor de la clase
            pred = n.predecir([X[i]])[0]
            actual = y[i][0]

            # ajusto
            n.ajustar(self.alfa, X[i], pred[0], actual)

            # sumo el error cuadrado al e
            error = actual - pred[0]
            if error != 0.0:
                self.cambios = True
            error_acum += error ** 2

        self.error_avg = error_acum / X.shape[0]


    def entrenar(self, n, X, y):
        """
        Entrena la neurona recibida.
        :param n: La neurona a entrenar.
        :param X: El conjunto de entrenamiento.
        :param y: El conjunto de clases.
        """
        logging.info('Aplicando Regla Delta con Alfa = %10.12f, Error Máximo = %10.12f, Límite iteraciones = %d' %
                     (self.alfa, self.error_ok, self.max_ite))

        self.error_avg = 0.0
        error_act = 1

        for ite in range(self.max_ite):
            self.iterar_dataset(n, X, y)

            error_ant = error_act
            error_act = self.error_avg
            delta_err = abs(error_act - error_ant)
            logging.debug('Iteración: %d, Error cuadrático medio %10.12f, Delta Error: %10.12f' % (ite, error_act, delta_err))

            salir = self.cambios if isinstance(n, Perceptron) else delta_err <= self.error_ok
            if salir:
                break

        if not salir: raise AssertionError(
            'No se alcanzó un error aceptable, se recomienda probar otros valores alfa o incrementar la cantidad de iteraciones')

        logging.info('Nivel de error aceptable alcanzado en %d iteraciones: %10.12f < %10.12f' %
                     (ite, delta_err, self.error_ok))
