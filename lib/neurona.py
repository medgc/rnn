import numpy as np
import logging

class Neurona:
    weights = None
    b = None
    fn = None

    def __init__(self, fn, cant_entradas, usa_b=False):
        """
        Inicialización de la neurona.
        :param fn: Función de transferencia a utilizar.
        :param cant_entradas: Cantidad de entradas de la neurona.
        :param usa_b: Indica si debe usar termino independiente.
        """
        self.fn = fn

        # pongo los weights en 0
        self.weights = np.random.uniform(-0.5, 0.5, cant_entradas)
        if usa_b:
            self.b = np.random.uniform(-0.5, 0.5)

    def usa_b(self):
        """
        Indica si esta neurona utiliza el término independiente o no.
        :return: True si lo utiliza, False si no.
        """
        return self.b is not None

    def get_weights(self):
        """
        Devuelve el vector de pesos para cada entrada.
        :return: Vector de pesos.
        """
        return self.weights

    def get_b(self):
        """
        Devuelve el valor de b.
        :return: Valor de b.
        """
        return self.b


    def get_fn(self):
        """
        Devuelve la funcion de transferencia asignada.
        :return: La funcion asignada.
        """
        return self.fn

    def __str__(self):
        return 'Neurona: {W: %s, b: %s, fn: %s}' % (
            np.array2string(np.asarray(self.weights), formatter={'float':lambda x: '%.2f' % x}),
            ('%f' % self.b) if self.usa_b() else 'N/A',
            self.fn.__class__.__name__)


    def ajustar(self, alfa, features, pred, actual):
        """
        Ajusta los pesos de la neurona.
        :param alfa: Velocidad de entrenamiento.
        :param grad_weights: Gradiente del error para las entradas.
        :param grad_b: Gradiente del error para el termino independiente.
        """
        error = actual - pred

        # calculo el gradiente por la derivada de la funcion
        grad_weights = -2 * error * self.fn.evaluar_df(pred) * features
        grad_b = -2 * error * self.fn.evaluar_df(pred)

        # ajusto
        self.weights -= alfa * grad_weights
        if self.usa_b():
            self.b = self.b - alfa * grad_b


    def predecir(self, X):
        """
        Predice los y para cada elemento en X.
        :param X: Array de vectores donde cada componente es un feature de entrada.
        :return: Array de vectores donde cada elemento es el resultado predicho.
        """
        result = []

        for item in X:
            neta = np.dot(self.weights, item) + (self.b if self.usa_b() else 0)
            result.append([self.fn.evaluar_f(neta)])

        return result

