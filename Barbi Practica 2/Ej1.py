# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 11:21:07 2020

@author: User
"""
#Práctica 2 EJ1
import pandas as pd
import numpy as np
from sklearn import *
from RN_Perceptron import *

#Datos
datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/datos/hojas.csv')
#datos = datos.sort_values(by='Clase')

titulos = list(datos.columns.values)

#--- DATOS DE ENTRENAMIENTO ---
entradas = np.array(datos.iloc[:,0:2])

# convirtiendo los atributos nominales en numericos

normalizarEntrada = 1  # 1 si normaliza; 0 si no
if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    entradas = min_max_scaler.fit_transform(entradas)

#--- SALIDA BINARIA ---
opciones = datos['Clase'].unique()
salida = datos['Clase'] == opciones[1]  #es boolean
salida = np.array(salida * 1)  #lo convierte en binario
# hoja=0 helecho=1

alfa = 0.01
MAX_ITE = 350
[W, b, ite] = entrena_Perceptron(entradas, salida,alfa, MAX_ITE, 1, titulos[0:2])    
yTrain = aplica_Perceptron(entradas, W, b)


print("cantidad de aciertos en el entrenamiento:", np.sum(yTrain==salida))
print("cantidad de errores en el entrenamiento:", np.sum(yTrain!=salida))        
      
# -- El perceptron ya está entrenado ---
# W y b determinan la recta que separa los ejemplos 

# Leer FrutasTest.csv y ver que responde el perceptrón
#datosTest = pd.read_csv("datos/FrutasTest.csv")
#salidaTest = datosTest['Clase'] == opciones[1]  #es boolean
#salidaTest = np.array(salidaTest * 1)  #lo convierte en binario
xTest = np.array([(770,5000)])
if normalizarEntrada:
    xTest = min_max_scaler.transform(xTest)

# Calcular las respuestas del perceptron
yTest = aplica_Perceptron(xTest,W,b)

ytest_manual= W[0]*xTest[0][0]+W[1]*xTest[0][1]+b
