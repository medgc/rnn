# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 11:59:40 2020

@author: User
"""
#Práctica 2 EJ2
import pandas as pd
import numpy as np
from sklearn import *
from RN_Perceptron import *

#Datos
datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/datos/sonar.csv')
#datos = datos.sort_values(by='Clase')

titulos = list(datos.columns.values)

#--- DATOS DE ENTRENAMIENTO ---
entradas = np.array(datos.iloc[:,:-1])

# convirtiendo los atributos nominales en numericos

normalizarEntrada = 0  # 1 si normaliza; 0 si no
if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    entradas = min_max_scaler.fit_transform(entradas)

#--- SALIDA BINARIA ---
opciones = datos['class'].unique()
salida = datos.iloc[:,-1] == opciones[1]  #es boolean
salida = np.array(salida * 1)  #lo convierte en binario
# rock=0 mine=1

# Entrenamiento
#Separo en train y test
entradas, xTest, salida, salidaTest = model_selection.train_test_split(entradas,salida,test_size=0.20, random_state=42)


alfa = 0.01
MAX_ITE = 350
[W, b, ite] = entrena_Perceptron(entradas, salida,alfa, MAX_ITE, 1, titulos[0:2])    
yTrain = aplica_Perceptron(entradas, W, b)


print("cantidad de aciertos en el entrenamiento:", np.sum(yTrain==salida))
print("cantidad de errores en el entrenamiento:", np.sum(yTrain!=salida))        
      
# -- El perceptron ya está entrenado ---
# W y b determinan la recta que separa los ejemplos 

if normalizarEntrada:
    xTest = min_max_scaler.transform(xTest)

# Calcular las respuestas del perceptron
yTest = aplica_Perceptron(xTest,W,b)

print("% de aciertos (train):", metrics.accuracy_score(salida,yTrain))
print("Matriz de confusion (train)\n", metrics.confusion_matrix(salida,yTrain))

print("% de aciertos (test):", metrics.accuracy_score(salidaTest,yTest))
print("Matriz de confusion (test)\n", metrics.confusion_matrix(salidaTest,yTest))
        
    
