import numpy as np
import pandas as pd
import logging
import os
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from regladelta import ReglaDelta
from transferencia import LogSig
from transferencia import TanSig
from neurona import Neurona

logging.basicConfig(format='[%(levelname)s] %(message)s')
logging.getLogger().setLevel(logging.INFO)


#
# Ejercicio 4
#
# Se ha realizado un análisis químico a tres tipos distintos de vinos producidos en una misma región de Italia. El
# número de muestras considerado es el siguiente:
# * Tipo 1 -> 59 muestras
# * Tipo 2 -> 71 muestras
# * Tipo 3 -> 48 muestras
# El archivo Vinos.csv permite observar los resultados de este análisis. Cada fila representa una muestra distinta y
# está formada, en primer lugar, por el número del tipo al cual pertenece el vino analizado seguido por los 13 atributos
# que lo caracterizan.
# Por ejemplo, la siguiente fila:
# 2,12.29,3.17,2.21,18,88,2.85,2.99,.45,2.81,2.3,1.42,2.83,406
# es el resultado del análisis de un vino correspondiente al tipo 2 (1er. valor de la fila) seguido por 13 valores
# separados por comas que indican los niveles de las mediciones realizadas a dicho vino.
#



#
# c) Rehacer a) utilizando un perceptrón
#


DATA_DIR = os.path.join('data')
INPUT_FILE = os.path.join(DATA_DIR, 'Vinos.csv')
CORRIDAS = 10

logging.info("Cargando datos de %s." % INPUT_FILE)
df = pd.read_csv(INPUT_FILE, compression='infer', decimal='.', delimiter=',', encoding='ISO-8859-1')
logging.info("%d registros leidos." % df.shape[0])
df.columns = ['tipo', 'v0', 'v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9', 'v10', 'v11', 'v12']

#
# a) Entrene una neurona no lineal para clasificar los vinos de Tipo 1, utilizando como función de activación una
# función sigmoide con salida entre 0 y 1 (logsig). Separe los ejemplos en dos grupos: uno para entrenamiento y el otro
# para testeo.
# Realice 10 ejecuciones independientes utilizando el 50%, 60%, 70%, 80% y 90% de los ejemplos como entrenamiento y el
# resto como testeo. Para cada porcentaje, indique la cantidad promedio (de las 10 corridas) de ejemplos correctamente
# clasificados.
# Analice:
# * los parámetros del entrenamiento.
# * la necesidad de escalar los datos de entrada antes de comenzar a trabajar.
# * la incidencia del orden de los ejemplos al momento de entrenar la neurona.
#

def ejecutar_corrida(train_pct, X, y, scaler, fn, error_ok, alfa):

    # creo los conjuntos
    X_train, X_test, y_train, y_test = train_test_split(
        X if scaler is None else scaler.fit_transform(X),
        y,
        test_size=1 - train_pct, random_state=42)

    logging.info(
        "Entrenando el modelo con Train: %f registros, Test: %f registros" % (X_train.shape[0], X_test.shape[0]))

    # entreno el modelo
    neurona = Neurona(fn=fn, cant_entradas=df.shape[1] - 1, usa_b=True)
    entrenador = ReglaDelta(max_ite=5000, error_ok=error_ok, alfa=alfa)
    entrenador.entrenar(neurona, X_train, y_train)
    print("   Neurona obtenida: %s" % neurona)

    # Pruebo el modelo
    y_pred = neurona.predecir(X_test)
    accuracy = accuracy_score(
        np.array(y_test).reshape(len(y_test), ),
        np.array(y_pred).reshape(len(y_pred), ) > 0.5)

    return accuracy


def correr_pruebas(error_ok, alfa, scaler, fn):
    results_df = pd.DataFrame(columns=['train_pct', 'accuracy_avg'])

    for train_pct in [0.5, 0.6, 0.7, 0.8, 0.9]:
        print("*******************************************************************************")
        print("Iniciando ciclo con %2.2f %% para Train y %2.2f %% para Test" % (train_pct * 100, (1 - train_pct) * 100))

        accuracy_acum = 0
        for i in range(CORRIDAS):
            accuracy = ejecutar_corrida(
                train_pct,  df.iloc[:, 1:].values,
                (df.iloc[:, 0:1].values == 1) * 1, scaler, fn,
                error_ok, alfa
            )
            accuracy_acum += accuracy
            print("   Corrida: %d, Accuracy: %f\n----------" % (i, accuracy))

        accuracy_avg = accuracy_acum / CORRIDAS
        print("Accuracy promedio %f" % (accuracy_avg))
        results_df = results_df.append({'train_pct': train_pct, 'accuracy_avg': accuracy_avg}, ignore_index=True)

    return results_df



rta_4a_con_scaler = correr_pruebas(0.0001, 0.001, MinMaxScaler(), LogSig())
print(rta_4a_con_scaler)

"""
Accuracy promedio 0.944444
   train_pct  accuracy_avg
0        0.5      0.850562
1        0.6      0.918310
2        0.7      0.929630
3        0.8      0.900000
4        0.9      0.944444
"""

#
rta_4a_sin_scaler = correr_pruebas(0.0001, 0.001, None, LogSig())
print(rta_4a_sin_scaler)

"""
   train_pct  accuracy_avg
0        0.5      0.500000
1        0.6      0.500000
2        0.7      0.559259
3        0.8      0.477778
4        0.9      0.455556
"""


#
# Conclusiones 4.a:
# Se logra un error de entrenamiento < 10^(-4) en ~500 iteraciones con un alfa de 10^(-3).
# El accuracy del modelo tiende a incrementarse a medida que crece el tamaño del conjunto de entrenamiento.
# La performance del modelo cae al ~50% al dejar de escalar los datos.
#



#
# b) Rehacer a) utilizando como función de activación una función sigmoide con salida entre -1 y 1 (tansig).
#

rta_4b = correr_pruebas(0.0001, 0.001, MinMaxScaler(), TanSig())
print(rta_4b)

"""
   train_pct  accuracy_avg
0        0.5      0.969663
1        0.6      0.967606
2        0.7      0.966667
3        0.8      0.950000
4        0.9      0.950000
"""
