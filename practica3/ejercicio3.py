import numpy as np
import pandas as pd
import logging
import os
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from regladelta import ReglaDelta
from transferencia import Lineal
from neurona import Neurona

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

#
# Ejercicio 3
#
# Los datos del archivo Autos.csv se refieren al consumo de combustible del ciclo urbano en millas por galón (mpg) que
# se puede predecir en términos de 3 atributos discretos multivaluados y 5 atributos continuos.

DATA_DIR = os.path.join('data')
INPUT_FILE = os.path.join(DATA_DIR, 'Autos.csv')

logging.info("Cargando datos de %s." % INPUT_FILE)
df = pd.read_csv(INPUT_FILE, compression='infer', decimal=',', delimiter=';', encoding='ISO-8859-1')
logging.info("%d registros leidos." % df.shape[0])


#
# a) Calcule la matriz de correlación entre los atributos numéricos y determine cuál es el atributo más correlacionado
# con el atributo MPG.
#
pd.set_option('display.max_columns', None)
df_corr = df.corr()
campo = df_corr.loc[df_corr.mpg != 1, 'mpg'].abs().sort_values(ascending=False).head(n=1).index[0] # Peso
r = df_corr.loc['mpg', campo] # -0.8317409332443342
print ("Respuesta 3.a: El campo '%s' es el atributo más correlacionado (r = %f)" % (campo, r)) # Respuesta 3.a: El campo 'Peso' es el atributo más correlacionado (r = -0.831741)


#
# b) Calcule la recta de regresión para predecir el valor de MPG a partir del PESO del auto utilizando un modelo de
# regresión lineal.
#
peso_scaler = MinMaxScaler()
X_train, X_test, y_train, y_test = train_test_split(
    # X=df['Peso'].values.reshape(-1, 1),
    peso_scaler.fit_transform(df['Peso'].values.reshape(-1, 1)),
    df['mpg'].values.reshape(-1, 1),
    test_size=0.3, random_state=42
)
lr = LinearRegression().fit(X=X_train, y=y_train)
print("Respuesta 3.b: La recta de regresión es y = %fx + %f" % (lr.coef_[0], lr.intercept_)) # y = -0.076766x + 463.173644
print("RMSE Regresión: %f" % mean_squared_error(y_test, lr.predict(X_test)))

#
# c) Rehacer b) utilizando un combinador lineal.
#
neurona = Neurona(fn=Lineal(), cant_entradas=1, usa_b=True)
entrenador = ReglaDelta(max_ite=5000, error_ok=0.0000001, alfa=0.001)
entrenador.entrenar(neurona, X_train, y_train)

# Pruebo el modelo
print("Neurona obtenida: %s" % neurona)

pred = neurona.predecir(X_test)
print("RMSE neurona: %f" % mean_squared_error(y_test, pred))


#
# d) Utilice los atributos numéricos disponibles para predecir el valor del atributo MPG. Indique cuál es hiperplano
# obtenido.
#
print("Respuesta 3.d: El hiperplano obtenido es la recta en R^2 y = %fx + %f" % (neurona.get_weights()[0], neurona.get_b())) # y = 366.959511x
