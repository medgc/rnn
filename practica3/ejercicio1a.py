import numpy as np
import logging
from regladelta import ReglaDelta
from transferencia import Lineal
from neurona import Neurona

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

#
# Ejercicio 1
#
# Utilizar RN formada por una única neurona con función de transferencia f(x)=x (combinador lineal) para convertir un
# número binario de 3 dígitos en un número decimal. Realice el entrenamiento utilizando la Regla Delta. Al finalizar
# el entrenamiento deberá representar gráficamente la RN obtenida. Realice el entrenamiento de dos formas distintas
#

#
# Ejercicio 1.b
#
# Utilizando bias o W0
#

# Conjunto de entrenamiento
X_train = np.array([
    [0, 0, 0],
    [0, 0, 1],
    [0, 1, 0],
    [0, 1, 1],
    [1, 0, 0],
    [1, 0, 1],
    [1, 1, 0],
    [1, 1, 1]
])
y_train = [[0], [1], [2], [3], [4], [5], [6], [7]]

# Hago test = train
X_test = X_train
y_test = y_train


# Creo la neurona que voy a entrnar, uso funcion lineal
neurona = Neurona(fn=Lineal(), cant_entradas=X_train[0].shape[0], usa_b=True)

# Entreno con descenso de gradiente
entrenador = ReglaDelta(max_ite=5000, error_ok=0.000000001, alfa=0.01)
entrenador.entrenar(neurona, X_train, y_train)



# Pruebo el modelo
print("Neurona obtenida: %s" % neurona)
pred = neurona.predecir(X_test)
for i in range(len(X_test)):
    print("Predicción sobre %s: Esperado = %d, Prediccion =  %f" % (X_test[i], y_test[i][0], pred[i][0]))

