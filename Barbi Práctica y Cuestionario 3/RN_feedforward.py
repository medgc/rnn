import numpy as np
import grafica as gr

def evaluar(FUN, x):
    if (FUN=='tansig'):
        return (2.0 / (1+np.exp(-2*x)) - 1)
    elif (FUN=='logsig'):
        return (1.0/(1+np.exp(-x)))
    elif (FUN=='umbral'):
        return ( (x>0) * 1)
    else:
        return(x)
    
def evaluarDerivada(FUN,x):
    if (FUN=='tansig'):
        return (1-x**2)
    elif (FUN=='logsig'):
        #return (x*(1+np.dot(-1,x)))
        return (x*(1-x))
    else:
        return(1)
        
def entrena_Perceptron(X, T, alfa, MAX_ITE, dibuja=0, titulos=[]):
    # Tamaño de los datos de entrada y títulos
    nCantEjemplos = X.shape[0]  # nro. de filas
    nAtrib = X.shape[1]         #nro. de columnas

    # Inicializar la recta
    W = np.random.uniform(-0.5, 0.5, size=nAtrib)
    b = np.random.uniform(-0.5, 0.5)
    
    if dibuja: # graficar
        gr.dibuPtos(1, X, T, titulos)
        ph = gr.dibuRecta(X, W, b)
        
    hubo_cambio=True
    ite=0
    while (hubo_cambio and (ite<MAX_ITE)):
        hubo_cambio=False
        ite = ite + 1
        
        # para cada ejemplo
        for i in range(nCantEjemplos):
            # Calcular la T
            # neta=b
            # for j in range(nAtrib):
            #      neta = neta + W[j] * X[i,j]
            neta = b + sum(W * X[i,:])
            y = 1 * (neta>0)
    
            # Si no es correcta, corregir W y b  
            if not(y==T[i]):
                hubo_cambio=True
                #    actualizamos los pesos W y b
                # for j in range(nAtrib):
                #     W[j] = W[j] + alfa *(T[i]-y)*X[i,j]
                W = W + alfa *(T[i]-y)*X[i,:]    
                b = b + alfa *(T[i]-y)
                        
        if dibuja: # graficar
            print(ite)
            ph = gr.dibuRecta(X, W, b, ph)
            
    return([W,b,ite])
    
def neurona_predice(X, W, b, FUN):
    cantEjemplos = X.shape[0]
    if (len(X.shape)==1):   # es una serie
        nAtrib = 1
        X = np.array(X).reshape(-1,1)
    else:
        nAtrib = X.shape[1]
        
    neta = np.zeros(cantEjemplos)
    for e in range(cantEjemplos):
        neta[e] = b
        for j in range(nAtrib):
            neta[e] = neta[e] + W[j]*X[e,j]
    Y = evaluar(FUN, neta)        
     
    # ----  Otra forma de hacer lo mismo  ----
    #netas = W @ entradas.T + b
    #Y = evaluar(netas)
    
    if (FUN=='logsig'):
        Y = (Y>0.5) * 1
    if (FUN=='tansig'):
        Y = 2*((Y>0)*1)-1
    return(Y)

def entrena_NeuronaLineal(X,T,alfa, MAX_ITE, CotaError=0.001, verIte = 0, dibuja=0, titulos=[]):
    #X es una matriz que tiene los ejemplos por fila
    nCantEjemplos = X.shape[0]  # nro. de filas
    if (len(T.shape)==1):
        T = np.array(T).reshape(-1,1)
    if (len(X.shape)==1):   # es una serie
        entradas = 1
        X = np.array(X).reshape(-1,1)
    else:
        entradas = X.shape[1]         #nro. de columnas
    
    # Inicializar la recta
    W = np.random.uniform(-0.5,0.5, entradas)
    b = np.random.uniform(-0.5, 0.5)
    
    if (entradas==1) and dibuja: # graficar
        X_aux = np.array(X).reshape(-1,1)
        T_aux = np.array(T).reshape(-1,1)
        puntos = np.concatenate((X_aux, T_aux), axis=1)

        gr.dibuPtos(1, puntos, np.zeros(puntos.shape[0]), titulos)
        ph = gr.dibuRecta(puntos, np.array([W, -1],dtype=object),b)
    
    ite=0
    ErrorAnt = 1
    ErrorAVG = 0
    while (ite<MAX_ITE) and (abs(ErrorAVG-ErrorAnt) > CotaError):
        ErrorAnt = ErrorAVG
        # para cada ejemplo
        ErrorAVG = 0
        for i in range(nCantEjemplos):
            # Calcular la salida
            neta= np.dot(W,X[i,:]) + b
            Y = neta
            Error_i = T[i]-Y
            gradiente_W =  Error_i *  X[i,:] 
            gradiente_b =  Error_i 
            #    actualizamos los pesos W y b
            W = W + alfa * gradiente_W 
            b = b + alfa * gradiente_b 
            
            ErrorAVG = ErrorAVG + Error_i**2
            
            
        ErrorAVG = ErrorAVG / nCantEjemplos
        
        ite = ite + 1
        if (verIte>0) and (ite % verIte ==0) :  
            print(ite, ErrorAVG, abs(ErrorAVG-ErrorAnt))
        
        if dibuja: # graficar la recta
            ph = gr.dibuRecta(puntos, np.array([W, -1],dtype=object),b, ph)

    return(W,b,ite)


def entrena_NeuronaGradiente(X,T,alfa, MAX_ITE,FUN = 'logsig', CotaError=0.001, verIte = 0, dibuja=0, titulos=[]):
    #X es una matriz que tiene los ejemplos por fila
    nCantEjemplos = X.shape[0]  # nro. de filas
    if (len(X.shape)==1):   # es una serie
        entradas = 1
        X = np.array(X).reshape(-1,1)
    else:
        entradas = X.shape[1]         #nro. de columnas
    
    # Inicializar la recta
    W = np.random.uniform(-0.5,0.5,entradas)
    b = np.random.uniform(-0.5, 0.5)
    
    if dibuja: # graficar
        gr.dibuPtos(1, X, T, titulos)
        ph = gr.dibuRecta(X, W, b)

    if (len(T.shape)==1):
        T = np.array(T).reshape(-1,1)
    
    ite=0
    ErrorAnt =0
    ErrorAVG = 1
    while (ite<MAX_ITE) and (abs(ErrorAVG-ErrorAnt) > CotaError):
        ErrorAnt = ErrorAVG
        ErrorAVG = 0
        # para cada ejemplo
        for i in range(nCantEjemplos):
            # Calcular la salida
            neta= np.dot(W,X[i,:]) + b
            Y = evaluar(FUN, neta)
            Error_i = T[i]-Y
    
            gradiente_W =  Error_i * evaluarDerivada(FUN,Y) * X[i,:] 
            gradiente_b =  Error_i * evaluarDerivada(FUN,Y)
            #    actualizamos los pesos W y b
            W = W + alfa * gradiente_W 
            b = b + alfa * gradiente_b

            ErrorAVG = ErrorAVG + Error_i**2
           
        ErrorAVG = ErrorAVG / nCantEjemplos

        ite = ite + 1

        if (verIte>0) and (ite % verIte ==0) :  
            print(ite, ErrorAVG, abs(ErrorAVG-ErrorAnt))
        
        if dibuja: # graficar la recta
            ph = gr.dibuRecta(X, W, b, ph)
    return([W,b, ite])
        

def BPN_entrena(P,T,FunH, FunO, ocultas, alfa=0.05, MAX_ITE = 200, CotaError=0.01, verIte=50):
    mezcla = np.random.permutation(len(P))
    P = P[mezcla,:]
    T = T[mezcla,:]
    
    entradas = P.shape[1]
    salidas = T.shape[1]
    
    W1 = np.random.uniform(-0.5,0.5,[ocultas, entradas])
    b1 = np.random.uniform(-0.5,0.5, [ocultas,1])
    W2 = np.random.uniform(-0.5,0.5,[salidas, ocultas])
    b2 = np.random.uniform(-0.5,0.5, [salidas,1])
    
    NetasH = W1 @ P.T + b1
    SalidasH = evaluar(FunH, NetasH)
    NetasO = W2 @ SalidasH + b2
    SalidasO = evaluar(FunO, NetasO)
    totalError = np.mean((T.T - SalidasO)**2)

    ite = 0
    while (ite < MAX_ITE) and (totalError > CotaError):
         
        for p in range(len(P)):
            # propagar el ejemplo hacia adelante
            netasH = W1 @ P[p:p+1,:].T + b1
            salidasH = evaluar(FunH, netasH)
            netasO = W2 @ salidasH + b2
            salidasO = evaluar(FunO, netasO)
    
            # calcular los errores en ambas capas        
            ErrorSalida = T[p:p+1,:].T-salidasO
            deltaO = ErrorSalida * evaluarDerivada(FunO,salidasO)
            deltaH = evaluarDerivada(FunH,salidasH)*(W2.T @ deltaO)
    
            # corregir los todos los pesos      
            W1 = W1 + alfa * deltaH @ P[p:p+1,:] 
            b1 = b1 + alfa * deltaH 
            W2 = W2 + alfa * deltaO @ salidasH.T 
            b2 = b2 + alfa * deltaO 
            
        ite = ite + 1
        
        NetasH = W1 @ P.T + b1
        SalidasH = evaluar(FunH, NetasH)
        NetasO = W2 @ SalidasH + b2
        SalidasO = evaluar(FunO, NetasO)
        totalError = np.mean((T.T - SalidasO)**2)
    
        if (verIte>0) and ((ite % verIte)==0):
            print(ite, totalError)
    return(W1,b1,W2,b2,ite)
    
def BPN_predice(P,W1,b1,W2,b2,FunH, FunO):
    NetasH = W1 @ P.T + b1
    SalidasH = evaluar(FunH, NetasH)
    NetasO = W2 @ SalidasH + b2
    SalidasO = evaluar(FunO, NetasO)
    if (FunO == 'tansig'):
        salidaBinaria = 1 * (SalidasO>0)
        return(2 * salidaBinaria.T - 1)
       
    elif (FunO == 'logsig'):
        salidaBinaria = 1 * (SalidasO>0.5)
        
        return(salidaBinaria.T)
    else:
        return (SalidasO)    
 
    

