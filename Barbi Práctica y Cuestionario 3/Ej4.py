# -*- coding: utf-8 -*-
"""
Created on Sun Dec  6 20:58:52 2020

@author: User
"""
import pandas as pd
import numpy as np
from sklearn import *
from RN_feedforward import *
from sklearn import linear_model, model_selection

#Datos
df = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/datos/Vinos.csv', header=None)
df.isnull().sum()

entradas = np.array(df.iloc[:,1:14])

normalizarEntrada = 1  # 1 si normaliza; 0 si no
if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    entradas = min_max_scaler.fit_transform(entradas)

#--- SALIDA BINARIA ---
df.iloc[:,0].unique()
salida = df.iloc[:,0] == 1 #es boolean
salida = np.array(salida * 1)  #lo convierte en binario
test_size= 0.2

#Neurona no lineal
alfa = 0.05
MAX_ITE = 150
FunO='tansig'

if (FunO=='tansig'):
    salida = 2*salida-1

X_train, X_test, y_train, y_test = model_selection.train_test_split(entradas,salida, test_size=test_size)

exp = 1
resultado= pd.DataFrame(columns = ['Exp', 'W', 'b', 'Aciertos_Train', 'Aciertos_Test'])

while   exp <= 10:
    [W, b, ite] = entrena_NeuronaGradiente(X_train,y_train,alfa, MAX_ITE,FUN = FunO, 
                                  CotaError=0.001, verIte = 0, dibuja=0, 
                                  titulos=[])
    yTrain = neurona_predice(X_train, W, b, FunO)
    aciertos_train= np.sum(yTrain==y_train)
    yTest= neurona_predice(X_test, W, b, FunO)
    aciertos_test= np.sum(yTest==y_test)
    resultado= resultado.append({'Exp': exp, 'W' : W, 'b' : b, 'Aciertos_Train' : aciertos_train,
                      'Aciertos_Test': aciertos_test}, ignore_index = True)
    print(exp)
    exp += 1
 
#resultado.to_csv('resultado_50.csv',sep=";", index=False)
aciertos= resultado.Aciertos_Test.mean()/len(y_test)
report = metrics.classification_report(y_test,yTest)
print("Confusion matrix TEST:\n%s" % report)
print(aciertos)
