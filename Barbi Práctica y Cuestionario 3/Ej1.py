#Combinador lineal para pasar de un numero binario a uno decimal

# -*- coding: utf-8 -*-
"""
Created on Sat Nov 21 18:55:39 2020

@author: User
"""
import pandas as pd
import numpy as np
from grafica import *
import math
import os

os.getcwd()
os.chdir('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales')

# %% Sin bias
datos= pd.read_csv('Clase 3/Práctica 3/binarios.csv', sep=';')
Y = np.array(datos['numero'])
X = np.array(datos.iloc[:,:-1])
cantEj = X.shape[0]  # cantidad de ejemplos de entrada


W = np.random.uniform(-0.5, 0.5,size=X.shape[1])
b = np.random.uniform(-0.5, 0.5)

#Conbinador lineal
alfa = 0.3
MAX_ITE = 1000
ite = 0
E_ant = 1
ErrorAct= 0

while ((ite<MAX_ITE) and (math.fabs(E_ant - ErrorAct)>0.0000000001)):
    E_ant=ErrorAct
    ErrorAct= 0
    for e in range(cantEj):
        salida = W.dot(X[e])+b
        ErrorE = Y[e] - salida

        grad_b = -2 * ErrorE
        grad_W = -2 * ErrorE * X[e]
        
        b = b - alfa * grad_b
        W = W - alfa * grad_W
        
        ErrorAct = ErrorAct+ErrorE**2
    ErrorAct=ErrorAct/cantEj
    print("Iteración: ", ite," ,Error cuadratico medio: ",ErrorAct)
    ite = ite + 1

print ("Error cuadratico medio: ",ErrorAct," ,W: ",W," ,b: ",b, 'ite:', ite)

# %% Con bias
datos= pd.read_csv('Clase 3/Práctica 3/binarios.csv', sep=';')
datos['x0']=1
Y = np.array(datos['numero'])
X = np.array(datos.drop(['numero'], axis= 1))
cantEj = X.shape[0]  # cantidad de ejemplos de entrada


W = np.random.uniform(-0.5, 0.5,size=X.shape[1])

#Conbinador lineal
alfa = 0.3
MAX_ITE = 1000
ite = 0
E_ant = 1
ErrorAct= 0

while ((ite<MAX_ITE) and (math.fabs(E_ant - ErrorAct)>0.0000000001)):
    E_ant=ErrorAct
    ErrorAct= 0
    for e in range(cantEj):
        salida = W.dot(X[e])
        ErrorE = Y[e] - salida

        grad_W = -2 * ErrorE * X[e]
        
        W = W - alfa * grad_W
        
        ErrorAct = ErrorAct+ErrorE**2
    ErrorAct=ErrorAct/cantEj
    print("Iteración: ", ite," ,Error cuadratico medio: ",ErrorAct)
    ite = ite + 1

print ("Error cuadratico medio: ",ErrorAct," ,W: ",W, 'ite:', ite)
