# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 11:21:07 2020

@author: User
"""
#Práctica 3 EJ2
import pandas as pd
import numpy as np
from sklearn import *
from RN_feedforward import *

#Datos
datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/datos/hojas.csv')
#datos = datos.sort_values(by='Clase')

#--- DATOS DE ENTRENAMIENTO ---
entradas = np.array(datos.iloc[:,0:2])

# convirtiendo los atributos nominales en numericos

normalizarEntrada = 1  # 1 si normaliza; 0 si no
if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    entradas = min_max_scaler.fit_transform(entradas)

#--- SALIDA BINARIA ---
opciones = datos['Clase'].unique()
salida = datos['Clase'] == opciones[1]  #es boolean
salida = np.array(salida * 1)  #lo convierte en binario

# hoja=0 helecho=1
#Neurona no lineal
alfa = 0.01
MAX_ITE = 1000
FunO='tansig'

if (FunO=='tansig'):
    salida = 2*salida-1
    
[W, b, ite] = entrena_NeuronaGradiente(entradas,salida,alfa, MAX_ITE,FUN = FunO, 
                                  CotaError=0.001, verIte = 0, dibuja=0, 
                                  titulos=[])
yTrain = neurona_predice(entradas, W, b, FunO)


print("cantidad de aciertos en el entrenamiento:", np.sum(yTrain==salida))
print("cantidad de errores en el entrenamiento:", np.sum(yTrain!=salida))        
      
