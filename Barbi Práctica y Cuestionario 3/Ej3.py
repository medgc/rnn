# -*- coding: utf-8 -*-
"""
Created on Sun Dec  6 19:11:29 2020

@author: User
"""
#Práctica 3 EJ3
import pandas as pd
import numpy as np
from sklearn import *
from RN_feedforward import *
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score

#Datos
datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/datos/Autos_orig.csv', sep=";")
#datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/datos/Autos3.csv')
datos.isnull().sum()

#1- Correlación
sns.heatmap(datos.corr(), annot=True)

#Para ver como cambia la correl con eliminación de nulos
datos2= datos
datos2= datos2.dropna(axis=0, how='any')
sns.heatmap(datos2.corr(), annot=True)

datos3= datos
datos3['HP']=datos3.HP.replace(np.nan,datos3.HP.mean())
datos3.isnull().sum()
sns.heatmap(datos3.corr(), annot=True)

#2- Regresion Lineal
x = np.array(datos['Peso']).reshape((-1,1))
y = np.array(datos['mpg'])

model = LinearRegression().fit(x, y)
r_sq = model.score(x, y)
print('coefficient of determination:', r_sq)
print('intercept:', model.intercept_)
print('slope:', model.coef_)

#3- Combinador lineal
entradas = np.array(datos['Peso']).reshape((-1,1))
salida = np.array(datos['mpg'])

normalizarEntrada = 1 # 1 si normaliza; 0 si no
if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    entradas = min_max_scaler.fit_transform(entradas)

[W,b,ite] = entrena_NeuronaLineal(entradas, salida, alfa=0.01, MAX_ITE= 1000, CotaError=0.00001, dibuja=0, titulos=[], verIte = 10)
y_pred = neurona_predice(entradas,W,b,FUN='purelin')     

# Error Cuadrado Medio
print("Mean squared error: %.2f" % mean_squared_error(salida, y_pred))
# Puntaje de Varianza. El mejor puntaje es un 1.0
print('Variance score: %.2f' % r2_score(salida, y_pred))

#Combinador lineal con todas las vr. numéricas
entradas = np.array(datos.iloc[:,1:7])
salida = np.array(datos['mpg'])

normalizarEntrada = 1  # 1 si normaliza; 0 si no
if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    entradas = min_max_scaler.fit_transform(entradas)

[W,b,ite] = entrena_NeuronaLineal(entradas, salida, alfa=0.01, MAX_ITE= 1000, CotaError=0.00001, dibuja=0, titulos=[], verIte = 10)
y_pred = neurona_predice(entradas,W,b,FUN='purelin')     

# Error Cuadrado Medio
print("Mean squared error: %.2f" % mean_squared_error(salida, y_pred))
# Puntaje de Varianza. El mejor puntaje es un 1.0
print('Variance score: %.2f' % r2_score(salida, y_pred))
