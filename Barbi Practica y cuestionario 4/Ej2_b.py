""" Ej 2 parte b
El archivo Balance.csv tiene información sobre un experimento psicológico 
realizado para evaluar el aprendizaje en los niños. Cada fila de la tabla tiene las características de una balanza, referidas a la longitud de los brazos izquierdo y derecho de la balanza y al peso que hay en cada brazo, y un atributo que indica si la balanza se inclina al lado izquierdo (L), derecho (R), o está balanceada (B).
Rehaga lo solicitado en el Ejercicio 1 pero utilizando esta vez los 
datos del archivo Balance.csv."""

import numpy as np
from sklearn import preprocessing, metrics, model_selection
import pandas as pd
import RN_feedforward as rn

datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/Clase 4/Multiperceptrón-20201128/Practica 4 - Multiperceptron/Balance.csv.')
pd.unique(datos[' Balance'])

mapeo = {" Balance": {" L":0, " R":1, " B":2}}
datos.replace(mapeo, inplace=True)

# Tomamos todas las columnas menos la última
entradas = datos.iloc[:,:-1]

# convirtiendo la tabla en arreglo
entradas = np.array(entradas)

salidas = np.array(datos.iloc[:,-1])
nBalance = 3

#--- CONJUNTOS DE ENTRENAMIENTO Y TESTEO ---
#X_train, X_test, T_train, T_test = model_selection.train_test_split( \
#        entradas, salidas, test_size=0, random_state=42)

#Entreno con todos los ejemplos
X_train= entradas
T_train= salidas

T = np.zeros((len(T_train), nBalance))
for o in range(len(T_train)):
    T[o, T_train[o]]=1


normalizarEntrada = 1  # 1 si normaliza; 0 si no

if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    X_train = min_max_scaler.fit_transform(X_train)
    #X_test = min_max_scaler.transform(X_test)
    
    
FunH = 'logsig'
FunO = 'tansig'

if (FunO=='tansig'):
    T = 2*T-1

alfa = 0.2
CotaError = 1.0e-09
MAX_ITE = 1000
ocultas = 2

[W1,b1,W2,b2,ite] = rn.BPN_entrena(X_train,T,FunH, FunO, ocultas, alfa, MAX_ITE, CotaError)

y_predTrain = rn.BPN_predice(X_train,W1,b1,W2,b2,FunH, FunO)
y_pred = np.argmax(y_predTrain,axis=1)

report = metrics.classification_report(T_train,y_pred)
print("Confusion matrix:\n%s" % report)
MM = metrics.confusion_matrix(T_train,y_pred)
print("Confusion matrix:\n%s" % MM) 

y_predTest = rn.BPN_predice(X_test,W1,b1,W2,b2,FunH, FunO)
y_pred = np.argmax(y_predTest,axis=1)

report = metrics.classification_report(T_test,y_pred)
print("Confusion matrix TEST:\n%s" % report)
MM = metrics.confusion_matrix(T_test,y_pred)
print("Confusion matrix TEST:\n%s" % MM) 
