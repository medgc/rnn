# -*- coding: utf-8 -*-
"""
Ej 3
Entrene un multiperceptrón que sea capaz de clasificar un animal en una de las 7 clases.
Utilice el 70% de los ejemplos para entrenar y el 30% para realizar el testeo.
Realice al menos 10 ejecuciones independientes de la configuración seleccionada para respaldar sus afirmaciones referidas a la performance del modelo.
"""
import numpy as np
from sklearn import preprocessing, metrics, model_selection
import pandas as pd
import RN_feedforward as rn

datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/Clase 4/Multiperceptrón-20201128/Practica 4 - Multiperceptron/zoo.csv.')
datos.Clase.unique()

mapeo = {"Clase": {'Mamifero':0, 'Pez':1,'Ave':2, 
                   'Invertebrado':3, 'Insecto': 4, 
                   'Anfibio':5, 'Reptil':6}}
datos.replace(mapeo, inplace=True)

# Tomamos todas las columnas menos la última
entradas = datos.iloc[:,1:-1]

# convirtiendo la tabla en arreglo
entradas = np.array(entradas)

salidas = np.array(datos.iloc[:,-1])
nzoo = 7

#--- CONJUNTOS DE ENTRENAMIENTO Y TESTEO ---
#X_train, X_test, T_train, T_test = model_selection.train_test_split( \
#        entradas, salidas, test_size=0.30, random_state=42)

#Entreno con todos los ejemplos
X_train= entradas
T_train= salidas    
    
T = np.zeros((len(T_train), nzoo))
for o in range(len(T_train)):
    T[o, T_train[o]]=1


normalizarEntrada = 1  # 1 si normaliza; 0 si no

if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    X_train = min_max_scaler.fit_transform(X_train)
#    X_test = min_max_scaler.transform(X_test)
    
    
FunH = 'logsig'
FunO = 'tansig'

if (FunO=='tansig'):
    T = 2*T-1

alfa = 0.2
CotaError = 1.0e-09
MAX_ITE = 500
ocultas = 7

[W1,b1,W2,b2,ite] = rn.BPN_entrena(X_train,T,FunH, FunO, ocultas, alfa, MAX_ITE, CotaError)

y_predTrain = rn.BPN_predice(X_train,W1,b1,W2,b2,FunH, FunO)
y_pred = np.argmax(y_predTrain,axis=1)

report = metrics.classification_report(T_train,y_pred)
print("Confusion matrix:\n%s" % report)
MM = metrics.confusion_matrix(T_train,y_pred)
print("Confusion matrix:\n%s" % MM) 

y_predTest = rn.BPN_predice(X_test,W1,b1,W2,b2,FunH, FunO)
y_pred = np.argmax(y_predTest,axis=1)

report = metrics.classification_report(T_test,y_pred)
print("Confusion matrix TEST:\n%s" % report)
MM = metrics.confusion_matrix(T_test,y_pred)
print("Confusion matrix TEST:\n%s" % MM) 
