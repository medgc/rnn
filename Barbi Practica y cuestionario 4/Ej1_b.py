""" Ej 1 parte b
El archivo Vinos.csv tiene información referida a 13 características químicas y/o visuales de varias muestras de vinos pertenecientes a 3 clases distintas.
a) Con el 80% de los ejemplos, genere un árbol de clasificación utilizando sklearn.tree.DecisionTreeClassifier con los siguientes parámetros criterion='entropy', min_samples_split=4, min_impurity_decrease=0.2
Calcule la tasa de acierto sobre el 20% restante.
b) Utilice el 80% de los ejemplos del archivo Vinos.csv para entrenar un multiperceptrón que sea capaz que distinguir entre las 3 clases de vinos. Observe la tasa de acierto obtenida sobre el 20% restante.
Compare el árbol y la RN en lo que se refiere a las tasas de acierto obtenidas y a la facilidad de interpretar el modelo obtenido. ¿En qué circunstancias utilizaría cada uno de ellos?"""

import numpy as np
from sklearn import preprocessing, metrics, model_selection
import pandas as pd
import RN_feedforward as rn

datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/Clase 4/Multiperceptrón-20201128/Practica 4 - Multiperceptron/Vinos.csv', sep=';')

mapeo = {"Clase": {"Tipo1":0, "Tipo2":1, "Tipo3":2}}
datos.replace(mapeo, inplace=True)

# Tomamos todas las columnas menos la última
entradas = datos.iloc[:,1:14]

salidas = np.array(datos.iloc[:,0])
nVinos = 3

#--- CONJUNTOS DE ENTRENAMIENTO Y TESTEO ---
X_train, X_test, T_train, T_test = model_selection.train_test_split( \
        entradas, salidas, test_size=0.20, random_state=42)

T = np.zeros((len(T_train), nVinos))
for o in range(len(T_train)):
    T[o, T_train[o]]=1


normalizarEntrada = 1  # 1 si normaliza; 0 si no

if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    X_train = min_max_scaler.fit_transform(X_train)
    X_test = min_max_scaler.transform(X_test)
    
    
FunH = 'logsig'
FunO = 'tansig'

if (FunO=='tansig'):
    T = 2*T-1

alfa = 0.15
CotaError = 1.0e-09
MAX_ITE = 700
ocultas = 6

[W1,b1,W2,b2,ite] = rn.BPN_entrena(X_train,T,FunH, FunO, ocultas, alfa, MAX_ITE, CotaError)

y_predTrain = rn.BPN_predice(X_train,W1,b1,W2,b2,FunH, FunO)
y_pred = np.argmax(y_predTrain,axis=1)

report = metrics.classification_report(T_train,y_pred)
print("Confusion matrix:\n%s" % report)
MM = metrics.confusion_matrix(T_train,y_pred)
print("Confusion matrix:\n%s" % MM) 

y_predTest = rn.BPN_predice(X_test,W1,b1,W2,b2,FunH, FunO)
y_pred = np.argmax(y_predTest,axis=1)

report = metrics.classification_report(T_test,y_pred)
print("Confusion matrix TEST:\n%s" % report)
MM = metrics.confusion_matrix(T_test,y_pred)
print("Confusion matrix TEST:\n%s" % MM) 
