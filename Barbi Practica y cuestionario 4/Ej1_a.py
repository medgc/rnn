""" Ej 1 parte a
El archivo Vinos.csv tiene información referida a 13 características químicas y/o visuales de varias muestras de vinos pertenecientes a 3 clases distintas.
a) Con el 80% de los ejemplos, genere un árbol de clasificación utilizando sklearn.tree.DecisionTreeClassifier con los siguientes parámetros criterion='entropy', min_samples_split=4, min_impurity_decrease=0.2
Calcule la tasa de acierto sobre el 20% restante.
b) Utilice el 80% de los ejemplos del archivo Vinos.csv para entrenar un multiperceptrón que sea capaz que distinguir entre las 3 clases de vinos. Observe la tasa de acierto obtenida sobre el 20% restante.
Compare el árbol y la RN en lo que se refiere a las tasas de acierto obtenidas y a la facilidad de interpretar el modelo obtenido. ¿En qué circunstancias utilizaría cada uno de ellos?"""

from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn import tree, preprocessing, metrics

import pandas as pd
import numpy as np
import graphviz

datos = pd.read_csv('C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/Clase 4/Multiperceptrón-20201128/Practica 4 - Multiperceptron/Vinos.csv', sep=';')

# Tomamos todas las columnas menos la última
entradas = datos.iloc[:,1:14]

# convirtiendo la tabla en arreglo
entradas = np.array(entradas)

salidas = np.array(datos.iloc[:,0])

#--- CONJUNTOS DE ENTRENAMIENTO Y TESTEO ---
X_train, X_test, y_train, y_test = train_test_split( \
        entradas, salidas, test_size=0.20, random_state=42)
    
normalizarEntrada = 0  # 1 si normaliza; 0 si no

if normalizarEntrada:
    # Escala los valores entre 0 y 1
    min_max_scaler = preprocessing.MinMaxScaler()
    X_train = min_max_scaler.fit_transform(X_train)
    X_test = min_max_scaler.transform(X_test)
    
#--- modelo a entrenar ---
arbol = tree.DecisionTreeClassifier(criterion='entropy', \
                min_samples_split=4, min_impurity_decrease=0.02)

#-- ajustando el modelo a los datos de entrenamiento ---     
arbol.fit(X_train, y_train)
tree.plot_tree(arbol)

# -- analizando la precisión del modelo sobre los datos de entrenamiento --
y_pred = arbol.predict(X_train)

opciones =  pd.unique(datos['Clase']) # valores del atributo de clase
matConfusion = confusion_matrix(y_train, y_pred, labels=list(opciones))
print(matConfusion)

report = metrics.classification_report(y_train,y_pred)
print("Confusion matrix:\n%s" % report)

#-- analizando la precisión del modelo sobre los datos de testeo --
y_test_pred = arbol.predict(X_test)
matConfusion2= confusion_matrix(y_test, y_test_pred, labels=list(opciones))
print(matConfusion2)

report = metrics.classification_report(y_test,y_test_pred)
print("Confusion matrix:\n%s" % report)

#-- alternativa para conocer el accuracy del modelo --
print("accuracy on training set: %f" % arbol.score(X_train, y_train))
print("accuracy on test set: %f" % arbol.score(X_test, y_test))

#-- Graficando el árbol con Graphviz ---
nomAtribs = datos.iloc[:,1:14].columns  # nombres de los atributos
tree.export_graphviz(arbol, out_file="C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/Clase 4/Multiperceptrón-20201128/Practica 4 - Multiperceptron/VinosArbol.txt", class_names=list(opciones), \
                   feature_names=list(nomAtribs), impurity=False, filled=True)
        
with open("C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/Clase 4/Multiperceptrón-20201128/Practica 4 - Multiperceptron/VinosArbol.txt") as f:
    dot_graph = f.read()
dibu=graphviz.Source(dot_graph)
dibu.render("C:/Users/User/Documents/Austral - MEDGC/20 Redes Neuronales/Clase 4/Multiperceptrón-20201128/Practica 4 - Multiperceptron/VinosPDF")   #genera un pdf
