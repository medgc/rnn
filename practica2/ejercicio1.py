import numpy as np
import pandas as pd
import logging
import os
from regladelta import ReglaDelta
from perceptron import Perceptron
from sklearn.preprocessing import MinMaxScaler

logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)

#
# Ejercicio 1
#
# Se desea construir una Red Neuronal, formada por un único Perceptrón, para clasificar fotos de hojas diferenciando las
# que corresponden a helechos de las que no. A continuación, se muestran algunas de las imágenes que se utilizarán en el
# entrenamiento.
# Las imágenes ya han sido analizadas. El archivo hojas.csv contiene la cantidad de pixeles correspondientes al
# perímetro y el área de cada hoja. Utilice los ejemplos de hojas.csv para entrenar un perceptrón que permita reconocer
# cuando se trata de una hoja de helecho.

DATA_DIR = os.path.join('data')
INPUT_FILE = os.path.join(DATA_DIR, 'Hojas.csv')

logging.info("Cargando datos de %s." % INPUT_FILE)
df = pd.read_csv(INPUT_FILE, compression='infer', decimal='.', delimiter=',', encoding='ISO-8859-1')
logging.info("%d registros leidos." % df.shape[0])


#
# Ejercicio 1.a: Analice si es necesario escalar los datos de entrada.
#

df['Perimetro'].min() # 197.5807358
df['Perimetro'].max() # 1564.482323

df['Area'].min() # 980
df['Area'].max() # 12216

# Rta: hace falta escalar porque los ordenes de magnitud son muy diferentes


#
# Ejercicio 1.b: ¿Es posible clasificar todos los ejemplos correctamente ingresando todos los de una clase primero y
# luego todos los de la otra clase?
#

X = np.column_stack((
    MinMaxScaler().fit_transform(df[['Perimetro']]).reshape(12,),
    MinMaxScaler().fit_transform(df[['Area']]).reshape(12,)
))
y = np.array(1 * (df['Clase'] == 'Hoja')).reshape(df.shape[0], 1)

entrenador = ReglaDelta(max_ite=5000, error_ok=0.0000000001, alfa=0.0000001)

# entreno un perceptron con todo el conjunto
p1 = Perceptron(2, False)
entrenador.entrenar(p1, X, y)
print(str(p1)) # 'Perceptron: {W: [-0.02 0.08], b: None}'


# entreno un perceptron en 2 partes, la primera con las hojas la segunda con los helechos
hojas = df.Clase == 'Hoja'
p2 = Perceptron(2, False)
entrenador.entrenar(p2, X[hojas], y[hojas])
entrenador.entrenar(p2, X[~hojas], y[~hojas])
print(str(p2)) # 'Perceptron: {W: [-0.30 0.42], b: None}'

#
# Rta: No
#

#
# Ejercicio 1.c: A partir de los pesos del perceptrón entrenado, indique cuál es la función discriminante obtenida.
#

print(str(p1)) # 'Perceptron: {W: [-0.02 0.08], b: None}'

#
# Rta:
#

#
# Ejercicio 1.d: Calcule manualmente la respuesta del perceptrón si se ingresa una hoja con un perímetro de 770 pixeles
# y un área de 5000 pixeles.
#
